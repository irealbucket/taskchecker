//
//  Todo+CoreDataProperties.swift
//  ToDoListApp
//
//  Created by ohashi on 2016/08/16.
//  Copyright © 2016年 ohashi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Todo {

    @NSManaged var item: String?

}
