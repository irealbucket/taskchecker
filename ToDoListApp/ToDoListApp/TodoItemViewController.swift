//
//  TodoItemViewController.swift
//  ToDoListApp
//
//  Created by ohashi on 2016/08/16.
//  Copyright © 2016年 ohashi. All rights reserved.
//

import UIKit
import CoreData

class TodoItemViewController: UIViewController {
    
    @IBOutlet weak var todoField: UITextField!
        
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func save(sender: UIBarButtonItem) {
        let newTask = Todo.MR_createEntity()
        newTask?.item = todoField.text
        newTask?.managedObjectContext!.MR_saveToPersistentStoreAndWait()
        navigationController!.popViewControllerAnimated(true)
    }
}
